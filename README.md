# Gander compiler service

The `compiler-service` is no longer in use for the up-to-date branches in the other services. Instead, the `parser-service`, is used. The main reason being language support limitations. 

The compile service uses two different compilers, `ASTCompiler.java` and `SimpleExtended.java`. Both of them requires file content and creates a temporary file, compiles it, and returns the different information about the file. 

The `ASTCompiler.java` is used to retrieve all of the AST Nodes and their locations and was implemented to retrieve the AST Information to be able to retrieve the AST Tree.

The `SimpleExtended.java` retrieves the variable and method declarations and uses. It was implemented to be able to do syntax highlighting.

## Requirements

We are running the compiler on Java version 8. Make sure you have it installed on your computer. Newer versions might cause issues when trying to receive the AST tree.

For Windows users, the system variable should be set to:
- `JAVA_HOME : the path to the JDK `
- `PATH : %JAVA_HOME%\bin`

## Installing the Gander compiler service
- Clone the repository
- The compiler service uses Extendj as a git submodule, and needs to be retrieved. 
- To get the extendj compiler, `cd` into the folder and run
```git submodule init```
This should give a response `Submodule 'extendj' (https://bitbucket.org/extendj/extendj) registered for path 'extendj'`
- When the submodules are initialized, run the command 
```git submodule update```
- If all is done properly, the folder extendj should now contain the extendj library. 
- 

## Commands to use / test the service

Create jar: `./gradlew jar`

How to use an extension: `java -jar extension.jar data/filename.java`

How to run the service: `./gradlew run`

Run the commands: 

To get all uses: `curl -d "file content" -X POST localhost:8003/use`

To retrieve AST information: `curl -d "{"file" : "file content", "line" : "line content"}" -X POST localhost:8003/ast`

To retrieve AST information (example): `curl -d '{"file" : "class Simple{\n  public static void main(String args[]){\n  System.out.println(\"Hello Java\");\n  }\n}", "line" : "line content"}' -X POST localhost:8003/ast`