# Multi-stage Dockerfile to first build and then package service

# Build with gradle 7.6
FROM gradle:7.6-alpine
WORKDIR /usr/src/compiler-service
# Download and unpack version 8.1.2 of ExtendJ
RUN mkdir extendj && \
    cd extendj && \
    wget https://bitbucket.org/extendj/extendj/get/1886092c170d5dd6b5aba344658bb63aa2d5eefd.tar.bz2 -O - | \
    tar xj --strip-components 1 && \
    cd ..
# Copy relevant folders
COPY src src
COPY tools tools
COPY build.gradle .
COPY checkstyle.xml .
COPY gradle gradle
COPY gradlew .
COPY settings.gradle .
# Set gradle wrapper as executable and build the shadow-all.jar
RUN chmod 755 gradlew && \
    ./gradlew shadowJar

# Now package into service image, having just OpenJDK as base with no build tools required
FROM openjdk:8-alpine
WORKDIR /opt/compiler-service
# Copy shadow-all.jar from the build image and set it as the default command
COPY --from=0 /usr/src/compiler-service/build/libs/shadow-all.jar .
EXPOSE 8003
CMD ["java", "-jar", "shadow-all.jar"]