package org.extendj;

import java.text.ParseException;
import java.util.Map;
import org.extendj.ast.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/** A utils class for handling JSON Objects */
public class JSONUtils {

  /**
   * Parses the response as a JSON Object
   *
   * @param response Response from a request
   * @return JSONObject
   */
  public static JSONObject parseJSONObject(String response) throws ParseException {
    JSONParser parser = new JSONParser();
    JSONObject json;
    try {
      json = (JSONObject) parser.parse(response);

    } catch (Exception e) {
      return null;
    }
    return json;
  }

  /**
   * Parses the AST Nodes and locations from a Map to an JSON Array
   *
   * @param location Map<ASTNode, ASTLocation>
   * @return JSONArray
   */
  public static JSONArray makeASTLocationArray(Map<ASTNode, ASTLocation> locations) {
    JSONArray json = new JSONArray();
    try {
      for (ASTLocation location : locations.values()) {
        JSONObject obj = new JSONObject();
        obj.put("node", location.getNode().toString());
        obj.put("id", location.getID());
        obj.put("startposition", location.getStartPosition());
        obj.put("endposition", location.getEndPosition());
        json.add(obj);
      }
      return json;
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Parses the AST location to a JSON Object
   *
   * @param location ASTLocation
   * @return JSONObject
   */
  public static JSONObject makeASTObject(ASTLocation location) {
    JSONObject json = new JSONObject();
    try {
      json.put("node", location.getNode().toString());
      json.put("id", location.getID());
      json.put("startposition", location.getStartPosition());
      json.put("endposition", location.getEndPosition());
      return json;
    } catch (Exception e) {
      return null;
    }
  }
}
