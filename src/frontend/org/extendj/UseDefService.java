package org.extendj;

import java.io.File;
import java.io.FileWriter;
import java.net.HttpURLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.extendj.ast.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

/** Class for retrieving AST Information about the sent in file */
public class UseDefService {
  private static final String CLASS_NAME = UseDefService.class.getName();
  private Logger logger;

  /**
   * @param logger the Logger object used to debug log
   */
  public UseDefService(Logger log) {
    this.logger = log;
    logger.info("UseDefService started");
  }

  /**
   * Retrieves the VarUses and MethodUses in the posted file.
   *
   * @param req the HTTP request object
   * @param res the HTTP response object
   * @return A JSON String containing both the variables and methods of the file.
   */
  public String postUse(Request req, Response res) {
    logger.entering(CLASS_NAME, "postUse");
    System.out.println("ENTERING POST USE");
    try {
      JSONObject obj = JSONUtils.parseJSONObject(req.body());
      String path = createTmpPath(obj.get("file").toString());
      if (path == null) {
        res.status(400);
        JSONObject error = new JSONObject();
        error.put("error", "could not create temporary file for file content");
        String errorText = error.toString();
        res.body(errorText);
        logger.severe(errorText);
        return errorText;
      }
      String[] args = {path};
      SimpleExtended comp = new SimpleExtended(logger);
      int i = comp.run(args);
      Map<Declarator, ArrayList<VarAccess>> varUses = comp.getVarUses();
      Map<ParameterDeclaration, ArrayList<VarAccess>> paramUses = comp.getParamUses();
      Map<MethodDecl, ArrayList<MethodAccess>> methodUses = comp.getMethodUses();
      JSONObject json = new JSONObject();
      methodToJSON(methodUses, json);
      varToJSON(varUses, json);
      paramToJSON(paramUses, json);
      String result = json.toString();
      res.body(result);
      res.status(HttpURLConnection.HTTP_OK);
      logger.exiting(CLASS_NAME, "postUse", result);
      return result;
    } catch (Exception e) {
      System.out.println("Error here : " + e.getMessage());
      System.out.println("COULD NOT RETRIEVE USE DEF INFORMATION");
    }
    return null;
  }

  /**
   * Creates a temporary file with the content
   *
   * @param content The content of the file
   * @return The path to the temporary file
   */
  private String createTmpPath(String content) {
    logger.entering(CLASS_NAME, "createTempPath");
    try {
      Path tmpFilePath = Files.createTempFile(null, ".java");
      File tmpFile = tmpFilePath.toFile();
      tmpFile.deleteOnExit();
      FileWriter fileWriter = new FileWriter(tmpFile);
      String pattern = Pattern.quote("\\" + "n");
      String[] arrOfContent = content.split(pattern);

      for (String c : arrOfContent) {
        fileWriter.write(c);
        fileWriter.write("\n");
      }
      fileWriter.close();
      logger.exiting(CLASS_NAME, "createTempPath", tmpFilePath.toString());
      return tmpFilePath.toString();

    } catch (Exception e) {
      logger.severe("message: " + e.getMessage());
      return null;
    }
  }

  /**
   * Turns the map of variables and their accesses into a JSON Object and stores it
   *
   * @param varUses the map of VarUses and VarDecls
   * @param json The JSON Object where the VarUses will be stored
   */
  private void varToJSON(Map<Declarator, ArrayList<VarAccess>> varUses, JSONObject json) {
    logger.entering(CLASS_NAME, "varToJSON");
    JSONArray variables = new JSONArray();

    for (Map.Entry<Declarator, ArrayList<VarAccess>> entry : varUses.entrySet()) {
      JSONObject info = new JSONObject();
      info.put("name", entry.getKey().getID());
      info.put("isVar", true);
      info.put("start", entry.getKey().getPosition());
      info.put("end", entry.getKey().getEndPosition());
      ArrayList<VarAccess> uses = entry.getValue();
      JSONArray useArray = new JSONArray();
      for (VarAccess access : uses) {
        JSONObject useObject = new JSONObject();
        useObject.put("start", access.getPosition());
        useObject.put("end", access.getEndPosition());
        useArray.add(useObject);
      }
      info.put("uses", useArray);
      variables.add(info);
    }
    json.put("variables", variables);
    logger.exiting(CLASS_NAME, "fieldToJSON", json);
  }

  /**
   * Turns the map of fields and their accesses into a JSON Object and stores it
   *
   * @param fieldUses the map of VarUses and VarDecls
   * @param json The JSON Object where the VarUses will be stored
   */
  private void paramToJSON(
      Map<ParameterDeclaration, ArrayList<VarAccess>> paramUses, JSONObject json) {
    logger.entering(CLASS_NAME, "fieldToJSON");
    JSONArray variables = new JSONArray();

    for (Map.Entry<ParameterDeclaration, ArrayList<VarAccess>> entry : paramUses.entrySet()) {
      JSONObject info = new JSONObject();
      info.put("name", entry.getKey().getID());
      info.put("isVar", true);
      info.put("start", entry.getKey().getPosition());
      info.put("end", entry.getKey().getEndPosition());
      ArrayList<VarAccess> uses = entry.getValue();
      JSONArray useArray = new JSONArray();
      for (VarAccess access : uses) {
        JSONObject useObject = new JSONObject();
        useObject.put("start", access.getPosition());
        useObject.put("end", access.getEndPosition());
        useArray.add(useObject);
      }
      info.put("uses", useArray);
      variables.add(info);
    }
    json.put("parameters", variables);
    logger.exiting(CLASS_NAME, "fieldToJSON", json);
  }

  /**
   * Turns the map of methods and their accesses into a JSON Object and stores it
   *
   * @param methodUses the map of MethodUses and MethodDecls
   * @param json The JSON Object where the VarUses will be stored
   */
  private void methodToJSON(Map<MethodDecl, ArrayList<MethodAccess>> methodUses, JSONObject json) {
    logger.entering(CLASS_NAME, "methodToJSON");
    JSONArray methods = new JSONArray();
    for (Map.Entry<MethodDecl, ArrayList<MethodAccess>> entry : methodUses.entrySet()) {
      JSONObject info = new JSONObject();
      info.put("name", entry.getKey().name());
      info.put("isVar", false);
      info.put("start", entry.getKey().getPosition());
      info.put("end", entry.getKey().getEndPosition());
      ArrayList<MethodAccess> uses = entry.getValue();
      JSONArray useArray = new JSONArray();
      for (MethodAccess access : uses) {
        JSONObject useObject = new JSONObject();
        useObject.put("start", access.getPosition());
        useObject.put("end", access.getEndPosition());
        useArray.add(useObject);
      }
      info.put("uses", useArray);
      methods.add(info);
    }
    json.put("methods", methods);
    logger.exiting(CLASS_NAME, "methodToJSON", methods);
  }

  /**
   * Retrieves the AST Information about a file.
   *
   * @param req the HTTP Request object
   * @param res the HTTP Response object
   * @return the found AST Information as a JSON String
   */
  public String getASTInformation(Request req, Response res) {
    logger.entering(CLASS_NAME, "getASTInformation");
    //debug
    System.out.println("ENTERING getASTInformation");
    try {
      JSONObject obj = JSONUtils.parseJSONObject(req.body());
      //debug
      System.out.println("obj: " + obj.toString());
      logger.info("obj: " + obj.toString());

      String path = createTmpPath(obj.get("file").toString());
      if (path == null) {
        //debug
        System.out.println("path is null");
        logger.severe("path is null");

        res.status(400);
        JSONObject error = new JSONObject();
        error.put("error", "could not create temporary file for ast retrieval");
        String errorText = error.toString();
        res.body(errorText);
        logger.severe(errorText);
        logger.exiting(CLASS_NAME, "getASTInformation", errorText);
        return errorText;
      }
      String[] args = {path};
      ASTCompiler comp = new ASTCompiler(logger);
      int i = comp.run(args);
      Map<ASTNode, ASTLocation> locations = comp.getLocationForNodesMap();
      //debug
      System.out.println("locations: " + locations.toString());
      logger.info("locations: " + locations.toString());

      JSONArray result = JSONUtils.makeASTLocationArray(locations);
      //debug
      System.out.println("result: " + result.toString());
      logger.info("result: " + result.toString());

      String resultText = comp.getJSONASTTree();
      //debug
      System.out.println("resultText: " + resultText);
      logger.info("resultText: " + resultText);

      res.body(resultText);
      res.status(HttpURLConnection.HTTP_OK);
      logger.exiting(CLASS_NAME, "getASTInformation", resultText);
      //debug
      System.out.println("EXITING getASTInformation");
      return resultText;
    } catch (ParseException e) {
      JSONObject error = new JSONObject();
      error.put("error", String.format("Parsing exception: %s", e.getMessage()));
      String errorText = error.toString();
      res.body(errorText);
      logger.severe(errorText);
      logger.exiting(CLASS_NAME, "getASTInformation", errorText);
      return errorText;
    }
  }

  /**
   * Finds the AST Node that is located at the specified position in the AST Tree.
   *
   * @param locations The map containing all of the AST Nodes and their corresponding AST Location
   * @param position The position in the format "line:col"
   * @return the AST Node on the specified location in the AST Tree.
   */
  private ASTLocation findLocation(Map<ASTNode, ASTLocation> locations, String position) {
    logger.entering(CLASS_NAME, "findLocation");
    for (ASTLocation loc : locations.values()) {
      if (isLocationInPosition(loc, position)) {
        logger.exiting(CLASS_NAME, "findLocation", loc);
        return loc;
      }
    }
    logger.severe("Could not find a location");
    logger.exiting(CLASS_NAME, "findLocation");
    return null;
  }

  /**
   * Helper method for findLocation().
   *
   * <p>Checks if the position is inside the AST Location.
   *
   * @param loc The ASTLocation
   * @param position The position in the format "line:col"
   * @return True if the ASTLocation is in the specified location. False if it is not.
   */
  private boolean isLocationInPosition(ASTLocation loc, String position) {
    logger.entering(CLASS_NAME, "isLocationInPosition");
    String[] arrOfPosArgs = position.split(":", 2);
    int line = Integer.parseInt(arrOfPosArgs[0]);
    int col = Integer.parseInt(arrOfPosArgs[1]);
    if (loc.startLine >= line && loc.endLine <= line && loc.startCol >= col && loc.endCol <= col) {
      logger.exiting(CLASS_NAME, "isLocationInPosition", true);
      return true;
    }
    logger.exiting(CLASS_NAME, "isLocationInPosition", false);
    return false;
  }
}
