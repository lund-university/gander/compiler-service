/* Copyright (c) 2005-2008, Torbjorn Ekman
 *               2011-2016, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.logging.Logger;
import org.extendj.ast.*;

/** Perform static semantic checks on a Java program. */
public class SimpleExtended extends Frontend {
  private Map<Declarator, ArrayList<VarAccess>> varUses;
  private Map<MethodDecl, ArrayList<MethodAccess>> methodUses;
  private Map<ParameterDeclaration, ArrayList<VarAccess>> paramUses;
  private static final String CLASS_NAME = SimpleExtended.class.getName();
  private Logger logger;

  /**
   * Entry point for the Java checker.
   *
   * @param args command-line arguments
   */
  public static void main(String args[]) {
    int exitCode = new SimpleExtended(App.setupLogger(0000)).run(args);
    if (exitCode != 0) {
      System.exit(exitCode);
    }
  }

  /** Initialize the Java checker. */
  public SimpleExtended(Logger log) {
    super("Simple Extended", ExtendJVersion.getVersion());
    varUses = new HashMap<Declarator, ArrayList<VarAccess>>();
    methodUses = new HashMap<MethodDecl, ArrayList<MethodAccess>>();
    paramUses = new HashMap<ParameterDeclaration, ArrayList<VarAccess>>();
    this.logger = log;
    logger.info(CLASS_NAME + " started");
  }

  /**
   * @param args command-line arguments
   * @return {@code true} on success, {@code false} on error
   * @deprecated Use run instead!
   */
  @Deprecated
  public static boolean compile(String args[]) {
    return 0 == new SimpleExtended(App.setupLogger(0000)).run(args);
  }

  /**
   * Run the Java checker.
   *
   * @param args command-line arguments
   * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
   */
  public int run(String args[]) {
    logger.entering(CLASS_NAME, "run", args);
    logger.exiting(CLASS_NAME, "run");
    return run(args, Program.defaultBytecodeReader(), Program.defaultJavaParser());
  }

  /**
   * Run the compiler with the supplied arguments.
   *
   * <p>Argument processing is done using the processArgs method. Override that if you want to
   * change the argument handling. Compilation options are stored in {@code program.options()}.
   *
   * <p>In normal operation this method is used to compile a set of source files. If the -help or
   * -version options are provided then comilation is skipped.
   *
   * <p>The return value indicates whether there were any errors during compilation.
   *
   * @return 0 on success, 1 on error, 2 on configuration error, 3 on system error, 4 on unhandled
   *     error
   */
  @Override
  public int run(String[] args, BytecodeReader reader, JavaParser parser) {
    logger.entering(CLASS_NAME, "run", args);
    program.resetStatistics();
    program.initBytecodeReader(reader);
    program.initJavaParser(parser);

    initOptions();
    int argResult = processArgs(args);
    if (argResult != 0) {
      return argResult;
    }

    Collection<String> files = program.options().files();
    try {
      for (String file : files) {
        program.addSourceFile(file);
      }

      TypeDecl object = program.lookupType("java.lang", "Object");
      if (object.isUnknown()) {
        // If we try to continue without java.lang.Object, we'll just get a stack overflow
        // in member lookups because the unknown (Object) type would be treated as circular.
        System.err.println(
            "Error: java.lang.Object is missing." + " The Java standard library was not found.");
        return EXIT_UNHANDLED_ERROR;
      }

    } catch (Exception e) {
      logger.severe(e.getMessage());
      return EXIT_UNHANDLED_ERROR;
    }
    Collection<CompilationUnit> work = new LinkedList<CompilationUnit>();
    Iterator<CompilationUnit> iter = program.compilationUnitIterator();
    while (iter.hasNext()) {
      CompilationUnit unit = iter.next();
      logger.fine("Iterating over " + unit.toString());
      logger.fine(unit.dumpTree());
      work.add(unit);
      try {
        for (int i = 0; i < unit.decls().size(); i++) {
          if (unit.decls().get(i) instanceof Declarator) {
            try {
              varUses.put(unit.decls().get(i), unit.decls().get(i).uses());
            } catch (Exception e) {
              logger.severe(e.getMessage() + " -- " + "--");
            }
          } //
        }
        for (MethodDecl methodDecl : unit.methodDecls()) {
          methodUses.put(methodDecl, methodDecl.calls());
        }
        for (int i = 0; i < unit.paramDecls().size(); i++) {
          try {
            if (unit.paramDecls().get(i) instanceof ParameterDeclaration) {
              paramUses.put(unit.paramDecls().get(i), unit.paramDecls().get(i).used());
            }
          } catch (Exception e) {
            logger.severe(
                e.getMessage()
                    + " ------- "
                    + unit.paramDecls().get(i).toString()
                    + " ---- "
                    + (unit.paramDecls().get(i) instanceof ParameterDeclaration));
          }
        }
        processCompilationUnit(unit);
      } catch (Exception e) {
        logger.severe(e.getMessage() + " 2");
        return EXIT_UNHANDLED_ERROR;
      }
    }
    logger.exiting(CLASS_NAME, "run");
    return EXIT_SUCCESS;
  }

  /**
   * Method to retrieve the VarDecls and VarUses.
   *
   * @return the Map containing the VarDecls and VarUses.
   */
  public Map<Declarator, ArrayList<VarAccess>> getVarUses() {
    logger.entering(CLASS_NAME, "getVarUses");
    logger.exiting(CLASS_NAME, "getVarUses", varUses);
    return varUses;
  }

  public Map<ParameterDeclaration, ArrayList<VarAccess>> getParamUses() {
    logger.entering(CLASS_NAME, "getVarUses");
    logger.exiting(CLASS_NAME, "getVarUses", paramUses);
    return paramUses;
  }

  /**
   * Method to retrieve the MethodDecls and MethodUses.
   *
   * @return the Map containing the MethodDecls and MethodUses.
   */
  public Map<MethodDecl, ArrayList<MethodAccess>> getMethodUses() {
    logger.entering(CLASS_NAME, "getMethodUses");
    logger.exiting(CLASS_NAME, "getMethodUses", methodUses);
    return methodUses;
  }
}
