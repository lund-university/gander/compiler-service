/* Copyright (c) 2005-2008, Torbjorn Ekman
 *               2011-2016, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj;

import java.util.*;
import java.util.logging.Logger;
import org.extendj.ast.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/** Perform static semantic checks on a Java program. */
public class ASTCompiler extends Frontend {
  private static final String CLASS_NAME = ASTCompiler.class.getName();
  private Logger logger;
  private Map<ASTNode, ASTLocation> locations;
  private JSONArray locationJSON;

  /**
   * Entry point for the Java checker.
   *
   * @param args command-line arguments
   */
  public static void main(String args[]) {
    int exitCode = new ASTCompiler(App.setupLogger(0000)).run(args);
    if (exitCode != 0) {
      System.exit(exitCode);
    }
  }

  /** Initialize the Java checker. */
  public ASTCompiler(Logger log) {
    super("AST Compiler", ExtendJVersion.getVersion());
    locations = new HashMap<ASTNode, ASTLocation>();
    locationJSON = new JSONArray();
    this.logger = log;
    logger.info(CLASS_NAME + " started");
  }

  /**
   * @param args command-line arguments
   * @return {@code true} on success, {@code false} on error
   * @deprecated Use run instead!
   */
  @Deprecated
  public static boolean compile(String args[]) {
    return 0 == new ASTCompiler(App.setupLogger(0000)).run(args);
  }

  /**
   * Run the Java checker.
   *
   * @param args command-line arguments
   * @return 0 on success, 1 on error, 2 on configuration error, 3 on system
   */
  public int run(String args[]) {
    logger.entering(CLASS_NAME, "run", args);
    logger.exiting(CLASS_NAME, "run");
    return run(args, Program.defaultBytecodeReader(), Program.defaultJavaParser());
  }

  /**
   * Run the compiler with the supplied arguments.
   *
   * <p>Argument processing is done using the processArgs method. Override that if you want to
   * change the argument handling. Compilation options are stored in {@code program.options()}.
   *
   * <p>In normal operation this method is used to compile a set of source files. If the -help or
   * -version options are provided then comilation is skipped.
   *
   * <p>The return value indicates whether there were any errors during compilation.
   *
   * @return 0 on success, 1 on error, 2 on configuration error, 3 on system error, 4 on unhandled
   *     error
   */
  @Override
  public int run(String[] args, BytecodeReader reader, JavaParser parser) {
    logger.entering(CLASS_NAME, "run", args);
    program.resetStatistics();
    program.initBytecodeReader(reader);
    program.initJavaParser(parser);

    initOptions();
    int argResult = processArgs(args);
    if (argResult != 0) {
      return argResult;
    }

    Collection<String> files = program.options().files();
    try {
      for (String file : files) {
        program.addSourceFile(file);
      }

      TypeDecl object = program.lookupType("java.lang", "Object");
      if (object.isUnknown()) {
        // If we try to continue without java.lang.Object, we'll just get a stack overflow
        // in member lookups because the unknown (Object) type would be treated as circular.
        System.err.println(
            "Error: java.lang.Object is missing." + " The Java standard library was not found.");
        return EXIT_UNHANDLED_ERROR;
      }
    } catch (Exception e) {
      logger.severe(e.getMessage());
      return EXIT_UNHANDLED_ERROR;
    }

    // Process source compilation units.
    Collection<CompilationUnit> work = new LinkedList<CompilationUnit>();
    Iterator<CompilationUnit> iter = program.compilationUnitIterator();
    while (iter.hasNext()) {
      CompilationUnit unit = iter.next();
      work.add(unit);
      try {
        findLocations(unit);
        createJson(unit);
        processCompilationUnit(unit);
      } catch (Exception e) {
        logger.severe(e.getMessage());
        return EXIT_UNHANDLED_ERROR;
      }
    }

    // Process library compilation units.

    /*Iterator<CompilationUnit> libraryIterator = program.libraryCompilationUnitIterator();
    while (libraryIterator.hasNext()) {
      CompilationUnit unit = libraryIterator.next();
      logger.fine("Iterating over " + unit.toString());
      work.add(unit);
      int result = processCompilationUnit(unit);
    }*/
    logger.exiting(CLASS_NAME, "run");
    return EXIT_SUCCESS;
  }

  /**
   * Retrieves the found AST nodes and their corresponding location in the code.
   *
   * @return A map with the ASTNodes and their ASTLocation (Map<ASTNode, ASTLocation>).
   */
  public Map<ASTNode, ASTLocation> getLocationForNodesMap() {
    logger.entering(CLASS_NAME, "getLocationForNodes");
    logger.exiting(CLASS_NAME, "getLocationForNodes", locations);
    return locations;
  }

  /**
   * Returns a mirror of the AST tree as a JSON String, where each entry has a reference to the
   * corresponding node in the AST. It also contains location details and the children for each
   * node.
   *
   * @return JSON String with the AST nodes and their locations
   */
  public String getJSONASTTree() {
    logger.entering(CLASS_NAME, "getJSONASTTree");
    logger.exiting(CLASS_NAME, "getJSONASTTree", locationJSON.toString());
    return locationJSON.toString();
  }

  /**
   * Method for finding the AST Locations.
   *
   * <p>Starts at the root node and calls a recursive method to find all of the nodes.
   *
   * <p>Stores the found locations in the attribute 'locations'.
   *
   * @param unit The root AST Node
   */
  private void findLocations(CompilationUnit unit) {
    logger.entering(CLASS_NAME, "findLocations", unit);
    ASTLocation root = findRecursive(unit);
    locations.put(unit, root);
    logger.exiting(CLASS_NAME, "findLocations");
  }

  /**
   * Recursive method to find all of the AST Nodes and their children to get a full AST Tree.
   *
   * @param node The AST Node we are currently on
   * @return The ASTLocation containing information about the node itself and its eventual children.
   */
  private ASTLocation findRecursive(ASTNode node) {

    // Creates a new AST Location with the node.
    ASTLocation loc =
        new ASTLocation(
            node,
            node.toString(),
            node.getLine(node.getStart()),
            node.getLine(node.getEnd()),
            node.getColumn(node.getStart()),
            node.getColumn(node.getEnd()));
    // If the node doesn't have any children, we have reached the bottom node in this iteration
    if (node.getNumChild() <= 0) {
      return loc;
    }

    // Otherwise we loop over each of the children and adds them to the children of the created
    // ASTLocation
    ArrayList<ASTLocation> children = new ArrayList<ASTLocation>();
    for (int i = 0; i < node.getNumChild(); i++) {
      children.add(findRecursive(node.getChild(i)));
    }
    loc.setChildren(children);
    return loc;
  }

  /**
   * Creates a JSON representation of the AST Tree and stores it in locatioJSON attribute.
   *
   * @param unit The node we want as a JSON representation
   */
  private void createJson(CompilationUnit unit) {
    logger.entering(CLASS_NAME, "createJson");
    JSONObject obj = new JSONObject();
    obj.put(locations.get(unit).simpleName(), createSingleJson(locations.get(unit).getNode()));
    locationJSON.add(obj);

    logger.exiting(CLASS_NAME, "createJson", locationJSON.toString());
  }

  /**
   * Helper method for createJson().
   *
   * <p>Creates a JSONObject for the specified AST Node.
   *
   * @param node the ASTNode we want as a JSONObject
   * @return the JSONObject of the ASTNode
   */
  private JSONObject createSingleJson(ASTNode node) {
    JSONObject obj = new JSONObject();
    obj.put("node", node.getClass().getSimpleName());
    obj.put("id", node.getID());
    obj.put("startposition", node.getPosition());
    obj.put("endposition", node.getEndPosition());
    if (node.getNumChild() <= 0) {
      return obj;
    }
    JSONArray children = new JSONArray();
    for (int i = 0; i < node.getNumChild(); i++) {
      children.add(createSingleJson(node.getChild(i)));
    }
    obj.put("children", children);
    return obj;
  }
}
